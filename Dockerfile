FROM registry.gitlab.com/kpmeen/docker-scala-sbt:scala_2.12

# initial install of av daemon
RUN echo "deb http://http.debian.net/debian/ $(lsb_release -cs) main contrib non-free" > /etc/apt/sources.list && \
    echo "deb http://http.debian.net/debian/ $(lsb_release -cs)-updates main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://security.debian.org/ $(lsb_release -cs)/updates main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y -qq \
    clamav-daemon \
    clamav-freshclam \
    libclamunrar7 \
    apt-utils \
    curl

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# initial update of av databases and clam run permissions
RUN \
    mkdir clam_tmp && \
    curl -L -o clam_tmp/main.cvd http://database.clamav.net/main.cvd && \
    # curl -L -o clam_tmp/daily.cvd http://database.clamav.net/daily.cvd && \
    curl -L -o clam_tmp/bytecode.cvd http://database.clamav.net/bytecode.cvd && \
    mv clam_tmp/*.cvd /var/lib/clamav && \
    chown clamav:clamav /var/lib/clamav/*.cvd && \
    mkdir /var/run/clamav && \
    chown clamav:clamav /var/run/clamav && \
    chmod 750 /var/run/clamav

# Configure clamd and freshclamd
RUN sed -i 's/^Foreground .*$/Foreground true/g' /etc/clamav/clamd.conf && \
    sed -i 's/^Foreground .*$/Foreground true/g' /etc/clamav/freshclam.conf

# volume provision
# VOLUME ["/var/lib/clamav"]

# port provision
EXPOSE 3310

# Define working directory
WORKDIR /root

# av daemon bootstrapping
ADD bootstrap.sh /
CMD ["/bootstrap.sh"]
