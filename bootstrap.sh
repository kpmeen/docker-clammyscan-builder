#!/bin/bash
# bootstrap clam av service and clam av database updater
set -m

# start in background
echo "Starting freshclam..."
freshclam -d &
echo "Starting clamd..."
clamd &

CLAMVERSION=`clamd --version`

echo "Started $CLAMVERSION"
