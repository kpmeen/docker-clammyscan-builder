[![pipeline status](https://gitlab.com/kpmeen/docker-clammyscan-builder/badges/master/pipeline.svg)](https://gitlab.com/kpmeen/docker-clammyscan-builder/commits/master)

# docker-clammyscan-builder
Docker image to use when building ClammyScan on CI. It comes pre-installed with clamav and a script to start it during CI builds.
